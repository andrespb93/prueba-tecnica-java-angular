import { EstudianteModel } from './../../../models/estudiante-model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EstudianteService } from 'src/app/service/estudiante.service';

@Component({
  selector: 'app-agregar-estudiante',
  templateUrl: './agregar-estudiante.component.html',
  styleUrls: ['./agregar-estudiante.component.css']
})
export class AgregarEstudianteComponent implements OnInit {

  nombre = '';

  constructor(
    private estudianteService: EstudianteService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onCreate(): void {
    const estudiante: EstudianteModel = {
      nombre: this.nombre,
    }
    this.estudianteService.save(estudiante).subscribe(
      data => {
        this.toastr.success('Estudiante Creado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onBack() {
    this.router.navigate(['estudiantes']);
  }

}
