import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProfesorModel } from 'src/app/models/profesor-model';
import { ProfesorService } from 'src/app/service/profesor.service';

@Component({
  selector: 'app-agregar-profesor',
  templateUrl: './agregar-profesor.component.html',
  styleUrls: ['./agregar-profesor.component.css']
})
export class AgregarProfesorComponent implements OnInit {

  nombre = '';

  constructor(
    private profesorService: ProfesorService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onCreate(): void {
    const profesor: ProfesorModel = {
      nombre: this.nombre,
    }
    this.profesorService.save(profesor).subscribe(
      data => {
        this.toastr.success('Estudiante Creado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onBack() {
    this.router.navigate(['profesores']);
  }
}
