import { CursoService } from './../../../service/curso.service';
import { CursoModel } from './../../../models/curso-model';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-lista-cursos',
  templateUrl: './lista-cursos.component.html',
  styleUrls: ['./lista-cursos.component.css']
})
export class ListaCursosComponent implements OnInit {

  lstCursos: CursoModel[] = [];

  constructor(
    private cursoService: CursoService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
    this.cargarCursos();
  }

  cargarCursos(): void {
    this.cursoService.lista().subscribe(
      data => {
        this.lstCursos = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  borrar(id: number) {
    this.cursoService.delete(id).subscribe(
      data => {
        this.toastr.success('Curso Eliminado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.cargarCursos();
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
      }
    );
  }
}
