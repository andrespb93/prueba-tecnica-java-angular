import { CursoService } from './../../../service/curso.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CursoModel } from 'src/app/models/curso-model';

@Component({
  selector: 'app-agregar-curso',
  templateUrl: './agregar-curso.component.html',
  styleUrls: ['./agregar-curso.component.css']
})
export class AgregarCursoComponent implements OnInit {

  grado: number = null;
  salon = '';

  constructor(
    private cursoService: CursoService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onCreate(): void {
    const curso: CursoModel = {
      grado: Number(this.grado),
      salon: this.salon
    }
    this.cursoService.save(curso).subscribe(
      data => {
        this.toastr.success('Curso Creado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onBack() {
    this.router.navigate(['cursos']);
  }

}
