import { AsignaturaModel } from './../../../models/asignatura-model';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AsignaturaService } from 'src/app/service/asignatura.service';

@Component({
  selector: 'app-lista-asignatura',
  templateUrl: './lista-asignatura.component.html',
  styleUrls: ['./lista-asignatura.component.css']
})
export class ListaAsignaturaComponent implements OnInit {
  
  lstAsignaturas: AsignaturaModel[] = [];

  constructor(
    private asignaturaService: AsignaturaService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
    this.cargarCursos();
  }
  
  cargarCursos(): void {
    this.asignaturaService.lista().subscribe(
      data => {
        this.lstAsignaturas = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  borrar(id: number) {
    this.asignaturaService.delete(id).subscribe(
      data => {
        this.toastr.success('Asignatura Eliminada', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.cargarCursos();
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
      }
    );
  }

}
