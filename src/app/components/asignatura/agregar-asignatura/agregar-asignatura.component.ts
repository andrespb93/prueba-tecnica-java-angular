import { ProfesorModel } from './../../../models/profesor-model';
import { AsignaturaModel } from './../../../models/asignatura-model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CursoModel } from 'src/app/models/curso-model';
import { AsignaturaService } from 'src/app/service/asignatura.service';

@Component({
  selector: 'app-agregar-asignatura',
  templateUrl: './agregar-asignatura.component.html',
  styleUrls: ['./agregar-asignatura.component.css']
})
export class AgregarAsignaturaComponent implements OnInit {

  nombre = '';
  profesor: ProfesorModel = null;
  curso: CursoModel = null;

  constructor(
    private asignaturaService: AsignaturaService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onCreate(): void {
    const asignatura: AsignaturaModel = {
      nombre: this.nombre,
      profesor: this.profesor,
      curso: this.curso
    }
    this.asignaturaService.save(asignatura).subscribe(
      data => {
        this.toastr.success('Asignatura Creada', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/']);
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onBack() {
    this.router.navigate(['asignaturas']);
  }

}
