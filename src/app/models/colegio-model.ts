import { CursoModel } from "./curso-model";

export class ColegioModel {
    id?:string;
    nombre: string;
    cursos: CursoModel[];
}