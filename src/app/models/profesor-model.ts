import { AsignaturaModel } from "./asignatura-model";

export class ProfesorModel {
    id?: string;
    nombre: string;
    asignaturas?: AsignaturaModel[];
}