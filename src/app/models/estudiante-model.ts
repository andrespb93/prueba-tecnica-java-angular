import { AsignaturaModel } from './asignatura-model';
export class EstudianteModel {
    id?: string;
    nombre: string;
    asignaturas?: AsignaturaModel[];
}