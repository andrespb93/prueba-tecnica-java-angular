import { AsignaturaModel } from "./asignatura-model";

export interface CursoModel {
    id?: string;
    grado: Number;
    salon: string;
    asignaturas?: AsignaturaModel[];
}