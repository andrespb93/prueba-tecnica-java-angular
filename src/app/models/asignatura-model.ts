import { CursoModel } from "./curso-model";
import { EstudianteModel } from "./estudiante-model";
import { ProfesorModel } from "./profesor-model";

export interface AsignaturaModel {
    id?: string;
    nombre: string;
    profesor: ProfesorModel;
    estudiantes?: EstudianteModel[];
    curso: CursoModel;
}