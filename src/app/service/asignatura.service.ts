import { AsignaturaModel } from './../models/asignatura-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CursoModel } from '../models/curso-model';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaService {

  asignaturaURL = 'http://localhost:8080/asignatura/';

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<AsignaturaModel[]> {
    return this.httpClient.get<AsignaturaModel[]>(this.asignaturaURL + 'lista');
  }

  public detail(id: number): Observable<AsignaturaModel> {
    return this.httpClient.get<AsignaturaModel>(this.asignaturaURL + `detail/${id}`);
  }

  public detailName(nombre: string): Observable<AsignaturaModel> {
    return this.httpClient.get<AsignaturaModel>(this.asignaturaURL + `detailname/${nombre}`);
  }

  public save(asignatura: AsignaturaModel): Observable<any> {
    return this.httpClient.post<any>(this.asignaturaURL + 'create', asignatura);
  }

  public update(id: number, asignatura: AsignaturaModel): Observable<any> {
    return this.httpClient.put<any>(this.asignaturaURL + `update/${id}`, asignatura);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.asignaturaURL + `delete/${id}`);
  }
}
