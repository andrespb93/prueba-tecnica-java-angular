import { EstudianteModel } from './../models/estudiante-model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {
  estudianteURL = 'http://localhost:8080/estudiante/';

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<EstudianteModel[]> {
    return this.httpClient.get<EstudianteModel[]>(this.estudianteURL + 'lista');
  }

  public detail(id: number): Observable<EstudianteModel> {
    return this.httpClient.get<EstudianteModel>(this.estudianteURL + `detail/${id}`);
  }

  public detailName(nombre: string): Observable<EstudianteModel> {
    return this.httpClient.get<EstudianteModel>(this.estudianteURL + `detailname/${nombre}`);
  }

  public save(estudiante: EstudianteModel): Observable<any> {
    return this.httpClient.post<any>(this.estudianteURL + 'create', estudiante);
  }

  public update(id: number, estudiante: EstudianteModel): Observable<any> {
    return this.httpClient.put<any>(this.estudianteURL + `update/${id}`, estudiante);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.estudianteURL + `delete/${id}`);
  }
}
