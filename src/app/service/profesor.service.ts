import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfesorModel } from '../models/profesor-model';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {
  profesorURL = 'http://localhost:8080/profesor/';

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<ProfesorModel[]> {
    return this.httpClient.get<ProfesorModel[]>(this.profesorURL + 'lista');
  }

  public detail(id: number): Observable<ProfesorModel> {
    return this.httpClient.get<ProfesorModel>(this.profesorURL + `detail/${id}`);
  }

  public detailName(nombre: string): Observable<ProfesorModel> {
    return this.httpClient.get<ProfesorModel>(this.profesorURL + `detailname/${nombre}`);
  }

  public save(profesor: ProfesorModel): Observable<any> {
    return this.httpClient.post<any>(this.profesorURL + 'create', profesor);
  }

  public update(id: number, profesor: ProfesorModel): Observable<any> {
    return this.httpClient.put<any>(this.profesorURL + `update/${id}`, profesor);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.profesorURL + `delete/${id}`);
  }
}
