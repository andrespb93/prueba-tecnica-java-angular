import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CursoModel } from '../models/curso-model';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  cursoURL = 'http://localhost:8080/curso/';

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<CursoModel[]> {
    return this.httpClient.get<CursoModel[]>(this.cursoURL + 'lista');
  }

  public detail(id: number): Observable<CursoModel> {
    return this.httpClient.get<CursoModel>(this.cursoURL + `detail/${id}`);
  }

  public detailName(nombre: string): Observable<CursoModel> {
    return this.httpClient.get<CursoModel>(this.cursoURL + `detailname/${nombre}`);
  }

  public save(curso: CursoModel): Observable<any> {
    return this.httpClient.post<any>(this.cursoURL + 'create', curso);
  }

  public update(id: number, curso: CursoModel): Observable<any> {
    return this.httpClient.put<any>(this.cursoURL + `update/${id}`, curso);
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.cursoURL + `delete/${id}`);
  }
}
