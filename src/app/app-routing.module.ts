import { EditarProfesorComponent } from './components/profesor/editar-profesor/editar-profesor.component';
import { EditarEstudianteComponent } from './components/estudiante/editar-estudiante/editar-estudiante.component';
import { AgregarEstudianteComponent } from './components/estudiante/agregar-estudiante/agregar-estudiante.component';
import { AgregarProfesorComponent } from './components/profesor/agregar-profesor/agregar-profesor.component';
import { EditarAsignaturaComponent } from './components/asignatura/editar-asignatura/editar-asignatura.component';
import { AgregarAsignaturaComponent } from './components/asignatura/agregar-asignatura/agregar-asignatura.component';
import { EditarCursoComponent } from './components/curso/editar-curso/editar-curso.component';
import { AgregarCursoComponent } from './components/curso/agregar-curso/agregar-curso.component';
import { HomeComponent } from './components/home/home.component';
import { ListaCursosComponent } from './components/curso/lista-cursos/lista-cursos.component';
import { ListaEstudianteComponent } from './components/estudiante/lista-estudiante/lista-estudiante.component';
import { ListaProfesorComponent } from './components/profesor/lista-profesor/lista-profesor.component';
import { ListaAsignaturaComponent } from './components/asignatura/lista-asignatura/lista-asignatura.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', component: HomeComponent},

  {path: 'cursos', component: ListaCursosComponent},
  {path: 'agregar-curso', component: AgregarCursoComponent},
  {path: 'editar-curso/:id', component: EditarCursoComponent},

  {path: 'asignaturas', component: ListaAsignaturaComponent},
  {path: 'agregar-asignatura', component: AgregarAsignaturaComponent},
  {path: 'editar-asignatura/:id', component: EditarAsignaturaComponent},

  {path: 'profesores', component: ListaProfesorComponent},
  {path: 'agregar-profesor', component: AgregarProfesorComponent},
  {path: 'editar-profesor/:id', component: EditarProfesorComponent},

  {path: 'estudiantes', component: ListaEstudianteComponent},
  {path: 'agregar-estudiante', component: AgregarEstudianteComponent},
  {path: 'editar-estudiante/:id', component: EditarEstudianteComponent},

  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
