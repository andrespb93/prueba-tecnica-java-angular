import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ListaAsignaturaComponent } from './components/asignatura/lista-asignatura/lista-asignatura.component';
import { EditarAsignaturaComponent } from './components/asignatura/editar-asignatura/editar-asignatura.component';
import { AgregarAsignaturaComponent } from './components/asignatura/agregar-asignatura/agregar-asignatura.component';
import { ListaCursosComponent } from './components/curso/lista-cursos/lista-cursos.component';
import { AgregarCursoComponent } from './components/curso/agregar-curso/agregar-curso.component';
import { EditarCursoComponent } from './components/curso/editar-curso/editar-curso.component';
import { ListaEstudianteComponent } from './components/estudiante/lista-estudiante/lista-estudiante.component';
import { AgregarEstudianteComponent } from './components/estudiante/agregar-estudiante/agregar-estudiante.component';
import { EditarEstudianteComponent } from './components/estudiante/editar-estudiante/editar-estudiante.component';
import { ListaProfesorComponent } from './components/profesor/lista-profesor/lista-profesor.component';
import { AgregarProfesorComponent } from './components/profesor/agregar-profesor/agregar-profesor.component';
import { EditarProfesorComponent } from './components/profesor/editar-profesor/editar-profesor.component';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    ListaAsignaturaComponent,
    EditarAsignaturaComponent,
    AgregarAsignaturaComponent,
    ListaCursosComponent,
    AgregarCursoComponent,
    EditarCursoComponent,
    ListaEstudianteComponent,
    AgregarEstudianteComponent,
    EditarEstudianteComponent,
    ListaProfesorComponent,
    AgregarProfesorComponent,
    EditarProfesorComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
